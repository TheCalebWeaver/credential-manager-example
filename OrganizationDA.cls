public inherited sharing class OrganizationDA {

	private static Organization Org {get {
		if (Org == null) {
			Org = getOrg();
		}
		return Org;
	}set;}

	public Boolean isSandbox() {
		return Org.IsSandbox;
	}

	private static Organization getOrg() {
		return [SELECT Id, IsSandbox FROM Organization][0];
	}
}