# Credential Manager Example #

Simple example of a way to store multiple sets of credentials in metadata and programatically change which ones are used depending on the environment.

This has the added benefit of being able to mock a production environment for testing by creating a mock OrganizationDA and hard coding the org returned.
