public inherited sharing class CredentialsDA {

	public OrganizationDA OrgDA {get {
		if (OrgDA == null) {
			OrgDA = new OrganizationDA();
		}
		return OrgDA;
	}set;}

	public Credentials__mdt getCredentials() {

		String orgType;
		if (OrgDA.isSandbox()) {
			orgType = 'Dev';
		} else {
			orgType = 'Prod';
		}

		List<Credentials__mdt> credentials = [SELECT Client__c, Secret__c, Oauth_Endpoint__c, Endpoint__c FROM Credentials__mdt WHERE Label = :orgType];
		if (!credentials.isEmpty()) {
			return credentials[0];
		} else {
			return null;
		}
	}
}